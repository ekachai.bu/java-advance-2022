package badcode;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegisterBusinessTest {

    @Test
    @DisplayName("ช้อมูล speaker = null จะโยน runtime exception กลับมา")
    public void register() {
        RegisterBusiness business = new RegisterBusiness();


        // assert exception with junit5
        Exception exception = assertThrows(RuntimeException.class, () -> {
            business.register(null,null);
        });
        assertEquals("Speaker is null",exception.getMessage());
    }

    @Test
    @DisplayName("First name = null throws First name is required.")
    public void case02() {
        RegisterBusiness business = new RegisterBusiness();

        // assert exception with junit5
        Exception exception = assertThrows(ArgumentNullException.class, () -> {
            business.register(null,new Speaker());
        });
        assertEquals("First name is required.",exception.getMessage());

    }

    @Test
    @DisplayName("Last name = null throws Last name is required.")
    public void case03() {
        RegisterBusiness business = new RegisterBusiness();

        Speaker speaker = new Speaker();
        speaker.setFirstName("firstName");

        // assert exception with junit5
        Exception exception = assertThrows(ArgumentNullException.class, () -> {
            business.register(null,speaker);
        });
        assertEquals("Last name is required.",exception.getMessage());
    }

    @Test
    @DisplayName("Email = null throws Email is required.")
    public void case04() {
        RegisterBusiness business = new RegisterBusiness();
        Speaker speaker = new Speaker();
        speaker.setFirstName("firstName");
        speaker.setLastName("lastName");
        // assert exception with junit5
        Exception exception = assertThrows(ArgumentNullException.class, () -> {
            business.register(null,speaker);
        });
        assertEquals("Email is required.",exception.getMessage());


    }

    @Test
    @DisplayName("Exp = null throws Can't save a speaker.")
    public void case05() {
        RegisterBusiness business = new RegisterBusiness();
        Speaker speaker = new Speaker();
        speaker.setFirstName("firstName");
        speaker.setLastName("lastName");
        speaker.setEmail("email@gmail.com");

        // assert exception with junit5
        Exception exception = assertThrows(SaveSpeakerException.class, () -> {
            business.register(null,speaker);
        });
        assertEquals("Can't save a speaker.",exception.getMessage());


    }


    @Test
    @DisplayName("Exp = null throws Can't save a speaker.")
    public void case06() {
        RegisterBusiness business = new RegisterBusiness();
        Speaker speaker = new Speaker();
        speaker.setFirstName("firstName");
        speaker.setLastName("lastName");
        speaker.setEmail("email@gmail.com");
        speaker.setExp(1);
        business.register(null,speaker);


    }
}
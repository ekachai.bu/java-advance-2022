package demo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HelloWorldTest {

    @Test
    @DisplayName("ทดสอบเรื่องอะไร อย่างไร ต้องได้อะไร")
    public void hi() {
    }

    @Test
    @DisplayName("ทดสอบเรื่องอะไร อย่างไร ต้องได้อะไร")
    public void case01() {
        HelloWorld helloWorld = new HelloWorld();
        String auctualResult = helloWorld.hi("Test");
        assertEquals("Hello Test",auctualResult);
    }

    @Test
    public void case02() {
    }
}
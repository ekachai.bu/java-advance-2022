package register;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DemoServiceTest {



    @Test
    public void compute() {
        DB db = new DBX();

        DemoService demoService = new DemoService(db); // constructor inject
        String actualResult = demoService.compute(1);
        assertEquals("Result = 2" ,actualResult);
    }
}

// interface
class DBX implements DB{
    @Override
    public int increaseOne(int id){
        return id+1;
    }
}

// override by method
/*
class DBX extends DB{
    @Override
    int increaseOne(int id){
        return id+1;
    }
}*/
